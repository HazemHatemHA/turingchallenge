
##TuringChallenge

In this challenge, you will build the backend of an e-commerce system which allows users to search, add items to their shopping cart, create orders, and checkout successfully.


# About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

https://laravel.com/docs/5.8/installation

* a whole document  API made by swagger => API_Documentation.json
* ER Diagram for database => t-shirt.png


## DDOS ATTACK
solution for DDOS ATTACK it's also Load balancer.

#Load Balancer
Load Balancer allows users to intelligently distribute traffic to a single IP across any number of servers using a number of different protocols.
##Round Robin
load balancer forwards a client request to each server in turn.
ser1 req1 ser2 req2 ser3 req3 ... etc
### when use it :
  A weight is assigned to each server based on criteria chosen by the site administrator


if a half of the daily active users come from the United States:
we will use :
  With the Source algorithm, the load balancer will select which server to use based on a hash of the source IP of the request, such as the visitor's IP address. This method ensures that a particular user will consistently connect to the same server.

#Test Requests im my device
 100 Req = > Time: 4.81 seconds, Memory: 42.00 MB,  4703 assertions
 1000 Req = > Time: 43.07 seconds, Memory: 40.00 MB, 47003 assertions
 10000 Req = > Time: 7.41 minutes, Memory: 40.00 MB, 470003 assertions




##References  Load balance
https://www.digitalocean.com/community/tutorials/what-is-load-balancing
