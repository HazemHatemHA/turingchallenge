@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
        <div class="col-md-12 row-block">
            <a href="{{ url('auth/facebook') }}" class="btn btn-lg btn-primary btn-block">
                <strong>Login With Facebook</strong>
            </a>
        </div>
    </div>
</div>
<!--
<script>
    window.fbAsyncInit = function() {
      FB.init({
        appId            : '830473684006095',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v3.3'
      });
    };
  </script>
  <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
-->
@endsection
