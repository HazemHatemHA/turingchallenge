

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <form action="{{route('stripeCharge')}}" method="POST">
            {!! csrf_field() !!}

            <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_gv5JkL6AdaUdYlMVBL9y0Uqd00fjIVZLOI"
                    data-amount="1999"
                    data-name="TuringChallenge Demo"
                    data-description="TuringChallenge with integrating Stripe"
                    data-image="https://cdn11.bigcommerce.com/s-44b1kojn/product_images/uploaded_images/stripe-payment-logo.png"
                    data-locale="auto"
                    data-currency="usd">
            </script>
        </form>

<script src="https://js.stripe.com/v3/"></script>
<style type="text/css">
    .StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
  </style>
<form action="{{route('stripeCharge')}}" method="post" id="payment-form">
        {!! csrf_field() !!}

  <div class="form-row">
        <br>
        description:<input type="text" name="description">
        <br>
        order_id:<input type="text" name="order_id">
        <br>
        amount:<input type="integer" name="amount">
        <br>
    <label for="card-element">
      Credit or debit card
    </label>
    <div id="card-element">
      <!-- A Stripe Element will be inserted here. -->
    </div>
    <br>
    <!-- Used to display form errors. -->
    <div id="card-errors" role="alert"></div>
  </div>

  <button>Submit Payment</button>
</form>
<script>
// Create a Stripe client.
var stripe = Stripe('pk_test_gv5JkL6AdaUdYlMVBL9y0Uqd00fjIVZLOI');

// Create an instance of Elements.
var elements = stripe.elements();

stripe.redirectToCheckout({
  items: [
    // Replace with the ID of your SKU
    {sku: 'sku_123', quantity: 1}
  ],
  successUrl: 'http://127.0.0.1:8000/stripe/charge',
  cancelUrl: 'http://127.0.0.1:8000/stripe/cancel',
}).then(function (result) {
  // If `redirectToCheckout` fails due to a browser or network
  // error, display the localized error message to your customer
  // using `result.error.message`.
});

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};



// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>

    </body>
</html>


