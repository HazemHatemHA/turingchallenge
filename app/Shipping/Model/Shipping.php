<?php

namespace TuringChallenge\Shipping\Model;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table = 'shipping';
    protected $primaryKey  = 'shipping_id';
    protected $fillable = ['shipping_type', 'shipping_cost', 'shipping_region_id'];


    public function shipping_region()
    {
        return $this->belongsTo('TuringChallenge\ShippingRegion\Model\ShippingRegion','shipping_region_id');
    }
    public function order()
    {
        return $this->hasMany('TuringChallenge\Order\Model\Order');
    }
}
