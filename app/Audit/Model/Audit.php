<?php

namespace TuringChallenge\Audit\Model;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = 'audit';
    protected $primaryKey  = 'audit_id';
    protected $fillable = ['order_id', 'created_on', 'message', 'code'];


    public function order()
    {
        return $this->belongsTo('TuringChallenge\Order\Model\Order');
    }
}
