<?php

namespace TuringChallenge\Payments\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;
use TuringChallenge\Errors\Error;
use TuringChallenge\Errors\Unauthorized;;
use TuringChallenge\Order\Model\Order;
use Stripe;
use Stripe\Error\Authentication;

class stripeController extends Controller
{


    public function __construct()
    { }


    public function form()
    {
        return view('stripePayment');
    }



    public function charge(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'stripeToken' => 'required|string',
            'order_id' => 'required|integer',
            'description' => 'required|string',
            'amount' => 'required|numeric',
            'currency' => 'string|nullable',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("VAL", $message, $field, 500);
            return $error->json();
        }
        $error;

        try {
            Stripe\Stripe::setApiKey("sk_test_lomdOfxbm7QDgZWvR82UhV6D");

            $stripeObject = Stripe\Charge::create([
                "amount" => ($request->amount * 100),
                "currency" => $request->currency ? $request->currency : "usd",
                "source" => $request->stripeToken,
                "description" => "payment from TuringChallenge",
                "metadata" => ["order_id" => $request->order_id]
            ]);
            $order = Order::find($request->order_id);
                  if($order){
            $order->auth_code = $stripeObject->id;
            $result = $order->save();
            if ($result) {
                return $stripeObject;
            }
}else{
$error = new  Error("INV_ORDER", "order ($request->order_id)does not exist", "model not found",200);
        return $error->json();
}
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $err = $e->getJsonBody()['error'];
            return $this->ErrorMessage($e, $err['code']);
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            return $this->ErrorMessage($e, "RL");
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            return $this->ErrorMessage($e, "REQ");
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            return $this->ErrorMessage($e, "AUT");
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            return $this->ErrorMessage($e, "NET");
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email

            return $this->ErrorMessage($e, "SB");
        } catch (\Exception $e) {
            // Something else happened, completely unrelated to Stripe
         $err  = $e->getMessage();
        $error = new  Error("GNR", $err, "Exception", $e->getCode());
        return $error->json();

        }
    }
    public function generateStripeToken()
    {
        \Stripe\Stripe::setApiKey("sk_test_lomdOfxbm7QDgZWvR82UhV6D");
        $token =  \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 12,
                "exp_year" => 2019,
                "cvc" => "314"
            )
        ));
        return $token;
    }

    private function ErrorMessage($e, $code)
    {

        $err  = $e->getJsonBody()['error'];
        $error = new  Error($code, $err['message'], $err['type'], $e->getHttpStatus());
        return $error->json();
    }


    public function webhooks(Request $request)
    {



        try {
            // my data storage location is project_root/storage/app/data.json file.
            $webhookTestJson = \Storage::disk('local')->exists('webhookTest.json') ? json_decode(\Storage::disk('local')->get('webhookTest.json')) : [];

            $data['type'] = $request->type;
            $object = $request->data["object"];
            $data['created'] = \Carbon\Carbon::createFromTimestamp($request->created);
            $data['amount'] = $object["amount"];
            $data['currency'] = $object["currency"];
            $data['amount_refunded'] = $object["amount_refunded"];
            $data['object_type'] = $object["object"];

            array_push($webhookTestJson, $data);

            \Storage::put('webhookTest.json', json_encode($webhookTestJson));
        } catch (\Exception $e) {
            \Storage::put('errorWebhookTest.json.txt', $e->getMessage());
            return ['error' => true, 'message' => $e->getMessage()];
        }




        return response()->json(['success' => 'webhook  received']);
    }
}
