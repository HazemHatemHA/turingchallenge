<?php
namespace TuringChallenge\Payments\Test\Unit;

use Illuminate\Foundation\Testing\WithFaker;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\Core\traits\Singleton;
use TuringChallenge\Order\Model\Order;
use TuringChallenge\Customer\Model\Customer;


class StripeTest extends TestCase
{
    use Singleton;


    function setUp()
    {
        parent::setUp();
    }

    public function testCheckOut()
    {
        \Stripe\Stripe::setApiKey("sk_test_lomdOfxbm7QDgZWvR82UhV6D");

        $token =  \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 12,
                "exp_year" => 2019,
                "cvc" => "314"
            )
        ));



        $item = Order::inRandomOrder()->first();
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('POST', '/stripe/charge', [
                'stripeToken' => $token,
                'order_id' => $item->order_id,
                'description' => 'Test Unit',
                'amount' => 33,
                'currency' => 'usd',
            ]);
            $response->assertOk();
        }
    }
}
