<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/stripe/form','stripeController@form');
Route::get('/stripe/generate','stripeController@generateStripeToken');

Route::post('/stripe/charge','stripeController@charge')->name('stripeCharge');

Route::post('/stripe/webhooks','stripeController@webhooks');

