<?php
namespace TuringChallenge\Attribute\Http\Resources;

use TuringChallenge\AttributeValue\Http\Resources\AttributeValueResource;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  IlluminateHttpRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        /* for one record

   return new AttributeResource(Attribute::find($id));

 for multi records

return AttributeResource::collection(Attribute::all());
 */
        return [
            'attribute_id'=>$this->attribute_id,
            'name' => $this->name,

         //   'attribute_value' => AttributeValueResource::collection($this->whenLoaded('attribute_value')),
        ];
    }
}
