<?php

namespace TuringChallenge\Attribute\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;

use TuringChallenge\Attribute\Model\Attribute;
use TuringChallenge\Attribute\Http\Resources\AttributeResource;

use TuringChallenge\ProductAttribute\Model\ProductAttribute;
use TuringChallenge\ProductAttribute\Http\Resources\ProductAttributeResource;



class attributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allAttributes()
    {
        //
        $data = AttributeResource::collection(Attribute::all());
        $data = $data->toArray($data);
        return $data;
    }

    public function allSpecificAttributes(Request $request, $attribute_id)
    {
        //
        $item = Attribute::find($attribute_id);
        if ($item == null) {
            $error = new NotFound($request);
            return $error->json();
        }
        return $item;
    }


    public function allAttributesValues(Request $request, $attribute_id)
    {
        $item = Attribute::find($attribute_id);
        if ($item == null) {
            $error = new NotFound($request);
            return $error->json();
        }


        $filteredAttributeID = $item->attribute_value->reject(function ($value, $key) {
            unset($value->attribute_id);
            return $value == null;
        });

        return  $filteredAttributeID;
    }

    public function allValuesAttributeProduct(Request $request, $product_id)
    {
        //
        $data = ProductAttribute::Where('product_id',$product_id)->get();

        $objects = $data->map(function ($item, $key) {
            $tmp['attribute_name'] =$item->attr_value->name;
            $tmp['attribute_value_id'] =$item->attribute_value->attribute_value_id;
            $tmp['attribute_value'] =$item->attribute_value->value;
            return $tmp;
        });
        return $objects->all();

    }
}
