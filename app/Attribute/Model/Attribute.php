<?php

namespace TuringChallenge\Attribute\Model;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //
    protected $table = 'attribute';
    protected $primaryKey  = 'attribute_id';
    protected $fillable = ['name'];
    public $timestamps = false;

    public function attribute_value()
    {
        return $this->hasMany('TuringChallenge\AttributeValue\Model\AttributeValue','attribute_value_id');
    }
}
