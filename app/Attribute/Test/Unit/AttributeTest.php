<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


use TuringChallenge\Attribute\Model\Attribute;
use TuringChallenge\AttributeValue\Model\AttributeValue;
use TuringChallenge\ProductAttribute\Model\ProductAttribute;


class AttributeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAllAttributes()
    {
        $response = $this->get('attributes');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([['attribute_id', 'name']]);
    }

    public function testAllSpecificAttributes()
    {
        $item = Attribute::inRandomOrder()->first();
        $response = $this->get('attributes/' . $item->attribute_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure(['attribute_id', 'name']);
    }

    public function testAllAttributesValues()
    {
        $item = AttributeValue::inRandomOrder()->first();
        $response = $this->get('attributes/values/' . $item->attribute_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([['attribute_value_id', 'value']]);
    }

    public function testAllValuesAttributeProduct()
    {
        $item = ProductAttribute::inRandomOrder()->first();
        $response = $this->get('attributes/inProduct/' . $item->product_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([['attribute_name', 'attribute_value_id', 'attribute_value']]);
    }
}
