<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'attributesController@allAttributes');


Route::get('/{attribute_id}', 'attributesController@allSpecificAttributes')->where('attribute', '[0-9]+');


Route::get('/values/{attribute_id}', 'attributesController@allAttributesValues')->where(['attribute_id' => '[0-9]+']);


Route::get('/inProduct/{product_id}', 'attributesController@allValuesAttributeProduct')->where(['product_id' => '[0-9]+']);

