<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/regions', 'ShippingRegionsController@allShippingsRegions');

Route::get('/regions/{shipping_region_id}', 'ShippingRegionsController@specificRegion')
    ->where(['shipping_region_id' => '[0-9]+']);
