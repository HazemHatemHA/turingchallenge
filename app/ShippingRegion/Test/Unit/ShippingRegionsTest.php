<?php

namespace TuringChallenge\ShippingRegion\Test\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\ShippingRegion\Model\ShippingRegion;

class ShippingRegionsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAllShippingsRegions()
    {
        $response = $this->get('shipping/regions/');
        $result = \json_decode($response->getContent());
        if (sizeof($result) > 0) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure([['shipping_region_id', 'shipping_region']]);
                return;
        }
        $this->checkResultIsNull($response);
    }



    public function testSpecificRegion()
    {
        $item = ShippingRegion::inRandomOrder()->first();
        if ($item) {
            $response = $this->get('shipping/regions/' . $item->shipping_region_id);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['shipping_id', 'shipping_type', 'shipping_cost', 'shipping_region_id']]
                );
                return;
        }

        $response = $this->get('shipping/regions/1');
        $this->checkResultIsNull($response);
    }

    public function checkResultIsNull($response)
    {
        $result = \json_decode($response->getContent());
        if (sizeof($result) > 0) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure(['shipping_region_id', 'shipping_region']);
        }
        $response->assertOk();
    }
}
