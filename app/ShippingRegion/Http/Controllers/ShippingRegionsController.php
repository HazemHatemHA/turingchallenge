<?php

namespace TuringChallenge\ShippingRegion\Http\Controllers;

use Illuminate\Http\Request;

use TuringChallenge\ShippingRegion\Model\ShippingRegion;
use TuringChallenge\Core\Http\Controllers\Controller;

class ShippingRegionsController extends Controller
{


    public function allShippingsRegions()
    {
        $data = ShippingRegion::all();
      return $data;

    }

    public function specificRegion(Request $request, $shipping_region_id)
    {
        $data = ShippingRegion::find($shipping_region_id);
        return $data->shipping;

    }
}
