<?php

namespace TuringChallenge\ShippingRegion\Model;

use Illuminate\Database\Eloquent\Model;

class ShippingRegion extends Model
{
    protected $table = 'shipping_region';
    protected $primaryKey  = 'shipping_region_id';
    protected $fillable = ['shipping_region'];


    public function shipping()
    {
        return $this->hasMany('TuringChallenge\Shipping\Model\Shipping','shipping_id');
    }
    public function customer()
    {
        return $this->hasMany('TuringChallenge\Customer\Model\Customer','customer_id');
    }
}
