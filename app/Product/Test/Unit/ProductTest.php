<?php
namespace TuringChallenge\Product\Test\Unit;

use Illuminate\Foundation\Testing\WithFaker;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\User\Model\User;

use TuringChallenge\Product\Model\Product;
use TuringChallenge\Department\Model\Department;
use TuringChallenge\Category\Model\Category;
use TuringChallenge\Customer\Model\Customer;
use TuringChallenge\Core\traits\Singleton;

class ProductTest extends TestCase
{
    use WithFaker, Singleton;


    function setUp()
    {
        parent::setUp();
    }




    public function testAllProducts()
    {
        $response = $this->get('products');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['count', 'rows']);
    }

    public function testSearchBox()
    { //search
        $response = $this->get('products/search?query_string=coat');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['count', 'rows']);
    }
    public function testPaging()
    {
        $response = $this->get('products/search?query_string=coat&page=2');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['count', 'rows']);
    }



    public function testSpecificProduct()
    {
        $item = Product::inRandomOrder()->first();
        $response = $this->get('products/' . $item->product_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['product_id', 'name', 'description', 'image', 'image_2', 'thumbnail', 'price', 'discounted_price']
            );
    }


    public function testProductsInCategory()
    {
        $item = Category::inRandomOrder()->first();
        $response = $this->get('products/inCategory/' . $item->category_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['count','rows']
            );
    }

    public function testProductsInDepartment()
    {

        $item = Department::inRandomOrder()->first();
        $response = $this->get('products/inDepartment/' . $item->department_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['count', 'rows']
            );
    }

    public function testProductDetails()
    {
        $item = Product::inRandomOrder()->first();
        if ($item) {
            $response = $this->get('products/' . $item->product_id . '/details');
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['name', 'description', 'price', 'discounted_price', 'image', 'image_2']
                );
        }
        return;
    }

    public function testProductLocations()
    {
        $item = Product::inRandomOrder()->first();
        if ($item) {
            $response = $this->get('products/' . $item->product_id . '/locations');
            $obj = json_decode($response->getContent());
            if (sizeof($obj) == 0) {
                $response->assertOk();
                return;
            }
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['category_id', 'category_name', 'department_id', 'department_name']
                );
        }
        return;
    }

    public function testProductReviews()
    {
        $item = Product::inRandomOrder()->first();
        if ($item) {
            $response = $this->get('products/' . $item->product_id . '/reviews');
            $obj = json_decode($response->getContent());
            if (sizeof($obj) == 0) {
                $response->assertOk();
                return;
            }
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['review', 'rating', 'created_on', 'name']]
                );
        }
        return;
    }

    public function testPostProductReviews()
    {

        $item = Product::inRandomOrder()->first();
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('POST', 'products/' . $item->product_id . '/reviews', [
                'review' => $this->faker->text,
                'rating' => $this->faker->numberBetween(1, 5),
            ]);
            $response->assertOk();
        }
    }
}
