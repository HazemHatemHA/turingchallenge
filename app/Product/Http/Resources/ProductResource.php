<?php
namespace TuringChallenge\Product\Http\Resources;
/*
use TuringChallenge\Review\Http\Resources\ReviewResource;
use TuringChallenge\ShoppingCart\Http\Resources\ShoppingCartResource;
use TuringChallenge\OrderDetail\Http\Resources\OrderDetailResource;
use TuringChallenge\ProductAttribute\Http\Resources\ProductAttributeResource;
use TuringChallenge\ProductCategory\Http\Resources\ProductCategoryResource;
*/

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  IlluminateHttpRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        /* for one record

   return new ProductResource(Product::find($id));

 for multi records

return ProductResource::collection(Product::all());
 */
        return [
            'product_id' => $this->product_id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,
            'image_2' => $this->image_2,
            'thumbnail' => $this->thumbnail,
            'price' => $this->price,
            'discounted_price' => $this->discounted_price,
            'display' => $this->display,
            /*
      //      'review' => ReviewResource::collection($this->whenLoaded('review')),
            'shopping_cart' => //ShoppingCartResource::collection($this->whenLoaded('shopping_cart')),
    //        'order_detail' => OrderDetailResource::collection($this->whenLoaded('order_detail')),
            'product_attribute' => //ProductAttributeResource::collection($this->whenLoaded('product_attribute')),
            'product_category' => //ProductCategoryResource::collection($this->whenLoaded('product_category')),
   //         'created_at' => $this->created_at,
    //        'updated_at' => $this->updated_at
    */
        ];
    }
}
