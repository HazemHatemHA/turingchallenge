<?php

namespace TuringChallenge\Product\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;

use TuringChallenge\Product\Model\Product;
use TuringChallenge\Product\Http\Resources\ProductResource;

use TuringChallenge\ProductCategory\Model\ProductCategory;
use TuringChallenge\Category\Model\Category;

use TuringChallenge\Review\Model\Review;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use TuringChallenge\Errors\Error;

class productsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['only' => ['postProductReviews']]);
    }
    private function createValidatorProducts(Request $request)
    {
        $optional = new \stdClass();
        $optional->page = 1;
        $optional->limit = 20;
        $optional->description_length = 200;
        $optional->code = null;


        if ($request->has('page')) {
            $optional->page = $request->input('page');
        }
        if ($request->has('limit')) {
            $optional->limit = $request->input('limit');
        }
        if ($request->has('description_length')) {
            $optional->description_length = $request->input('description_length');
        }

        $validator = \Validator::make((array)$optional, [
            'page' => 'integer',
            'limit' => 'integer',
            'description_length' => 'integer',
        ], [
            'integer' => 'The field :attribute  is must be integer.'
        ]);

        if ($validator->fails()) {
            return $this->ValidationFailed($validator);
        }
        return $optional;
    }
    public function allProducts(Request $request)
    {


        $optional = $this->createValidatorProducts($request);
        if ($optional->code) {
            return $optional;
        }

        $data = Product::whereRaw('LENGTH(description) < ' . $optional->description_length . '')->paginate($optional->limit, ['*'], 'page', $optional->page);
        return response()->json(
            [
                'count' => $data->total(),
                'rows' => $data->items()
            ]
        );
    }
    private $allowColumn = ['product_id', 'name', 'description', 'image', 'image_2', 'thumbnail', 'price', 'discounted_price', 'display'];

    public function productSearch(Request $request)
    {

        if ($request->query() > 0) {
            if ($request->has('query_string')) {
                $query_string = $request->input('query_string');

                $all_words = "off";
                $resultWhere = [];
                if ($request->has('all_words')) {
                    $flag = $request->input('all_words');
                    if ($flag == 'on' || $flag == 'off') {
                        $all_words = $flag;
                    } else {
                        $error = new Error("INV", "invalid format", "all words", 422);
                        return $error->json();
                    }
                }

                $optional = $this->createValidatorProducts($request);
                if ($optional->code) {
                    return $optional;
                }
                if ($query_string) {
                    $products = Product::search($query_string, $all_words)->paginate($optional->limit);

                    $products = $this->filterOnOptional($products, $optional);



                    return ['count' => $products->count(), 'rows' => $products];
                }
            }
            $error = new Error("PS", "The field query_string is empty.", "query_string", 422);
            return $error->json();
        }
    }

    private function filterOnOptional($items, $optional)
    {
        Paginator::currentPageResolver(function () use ($optional) {
            return $optional->page;
        });
        $items = $items->filter(function ($item, $key) use ($optional) {
            if (strlen($item->description) <= $optional->description_length) {
                $item->makeHidden(['image', 'image_2', 'display']);
                return $item;
            }
        });
        return $items;
    }

    public function specificProduct(Request $request, $product_id)
    {
        $data = Product::find($product_id);
        return $data;
    }

    public function productsInCategory(
        Request $request,
        $category_id
    ) {
        $optional = $this->createValidatorProducts($request);
        if ($optional->code) {
            return $optional;
        }

        $data = Product::whereRaw('LENGTH(description) < ' . $optional->description_length . '')->paginate($optional->limit, ['*'], 'page', $optional->page);

        $data = $data->filter(function ($item, $key) use ($category_id) {
            $category = $item->product_category->first();
            if ($category && $category->category_id == $category_id) {
                $item = $item->makeHidden(['image', 'image_2', 'display', 'product_category']);
                return $item;
            }
        });
        $count = $data->count();

        return  response()->json(['count' => $count, 'rows' => $data->all()]);
    }

    public function productsInDepartment(
        Request $request,
        $department_id
    ) {

        $optional = $this->createValidatorProducts($request);
        if ($optional->code) {
            return $optional;
        }

        $data = Product::whereRaw('LENGTH(description) < ' . $optional->description_length . '')->paginate($optional->limit, ['*'], 'page', $optional->page);
        $data = $data->filter(function ($item, $key) use ($department_id) {
            $product = $item->product_category->first();
            if ($product) {
                $department = $product->category;
                if ($department && $department->department_id == $department_id) {
                    $item = $item->makeHidden(['image', 'image_2', 'display', 'product_category']);
                    return $item;
                }
            }
        });
        $count = $data->count();

        return  response()->json(['count' => $count, 'rows' => $data->all()]);
    }

    public function productDetails(Request $request, $product_id)
    {
        $data = Product::find($product_id);
        $data->makeHidden(['thumbnail', 'display']);
        return $data;
    }

    public function productLocations(Request $request, $product_id)
    {
        $data =  Product::find($product_id)->product_category;
        $products = $data->map(function ($item, $key) {
            $item->makeHidden(['id', 'product_id', 'pivot']);
            $category = Category::find($item->category_id);
            $department  = Category::find($category->category_id)->department;
            $item['category_name'] = $category->name;
            $item['department_id'] = $department->department_id;
            $item['department_name'] = $department->name;
            return $item;
        });
        return $data;
    }

    public function productReviews(Request $request, $product_id)
    {

        $data =  Product::find($product_id);
        if ($data) {
            $data = $data->review;
            $data = $data->map(function ($item, $key) {
                $item->makeHidden(['customer_id', 'product_id', 'review_id', 'customer']);
                $item['name'] = $item->customer->name;
                return $item;
            });
            return $data;
        }
        $error = new Error("PR", "there'er no products", "productReviews", 400);
        return $error->json();
    }
    private function ValidationFailed(\Validator $validator)
    {
        $field = $validator->errors()->keys()[0];
        $message = $validator->errors()->first();
        $error = new Error("USR_02", $message, $field, 500);
        return $error->json();
    }

    public function postProductReviews(Request $request, $product_id)
    {
        $validator = \Validator::make($request->all(), [
            'review' => 'required|max:255',
            'rating' => 'required',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $this->ValidationFailed($validator);
        }


        $newRecord = Review::create(
            [
                'customer_id' => \Auth::guard('api')->user()->customer_id,
                'product_id' =>  $product_id,
                'review' => $request->review,
                'rating' => $request->rating,
                'created_on' => \Carbon\Carbon::now(),
            ]
        );
        \Log::info("add product review by " . \Auth::guard('api')->user()->email);
        return;
    }
}
