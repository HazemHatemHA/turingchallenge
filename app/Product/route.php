<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/{product_id}', 'productsController@specificProduct')
    ->where(['product_id' => '[0-9]+']);


Route::get('/', 'productsController@allProducts');

Route::get('/search', 'productsController@productSearch');



Route::get('/inCategory/{category_id}', 'productsController@productsInCategory');


Route::get('/inDepartment/{department_id}', 'productsController@productsInDepartment');

Route::get('/{product_id}/details', 'productsController@productDetails')
    ->where('product_id', '[0-9]+');


Route::get('/{product_id}/locations', 'productsController@productLocations')
    ->where('product_id', '[0-9]+');


Route::get('/{product_id}/reviews', 'productsController@productReviews')->where(['product_id' => '[0-9]+']);


Route::post('/{product_id}/reviews', 'productsController@postProductReviews')->where(['product_id' => '[0-9]+']);
