<?php

namespace TuringChallenge\Product\Model;

use Illuminate\Database\Eloquent\Model;
use TuringChallenge\Search\FullTextSearch;

class Product extends Model
{
    use FullTextSearch;
    protected $table = 'product';
    protected $primaryKey  = 'product_id';
    protected $fillable = ['name', 'description', 'image', 'image_2', 'thumbnail', 'price', 'discounted_price', 'display'];
    public $timestamps = false;

    protected $searchable = [
        'name',
        'description'
    ];


    public function review()
    {
        return $this->hasMany('TuringChallenge\Review\Model\Review', 'review_id');
    }
    public function shopping_cart()
    {
        return $this->hasMany('TuringChallenge\ShoppingCart\Model\ShoppingCart');
    }
    public function order_detail()
    {
        return $this->hasMany('TuringChallenge\OrderDetail\Model\OrderDetail');
    }
    public function product_attribute()
    {
        return $this->hasMany('TuringChallenge\ProductAttribute\Model\ProductAttribute');
    }
    public function product_category()
    {
        return $this->belongsToMany('TuringChallenge\ProductCategory\Model\ProductCategory', 'category', 'category_id', 'category_id');
    }
}
