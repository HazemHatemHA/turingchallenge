<?php

namespace TuringChallenge\Errors;

use Illuminate\Http\Request;
class Unauthorized extends Error
{

    public $code;
    public $message;
    public $field;

    function __construct($code,$message,$field)
    {
        $this->code = $code;
        $this->message = $message;
        $this->field = $field;
        unset($this->status);
    }
}
