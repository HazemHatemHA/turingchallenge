<?php

namespace TuringChallenge\Errors;

use Illuminate\Http\Request;

class Error
{
    public $code;
    public $message;
    public $field;
    public $status;


    function __construct($code,$message,$field,$status)
    {
        $this->code = $code;
        $this->message = $message;
        $this->field = $field;
        $this->status = $status;
    }

    public function json()
    {
        return json_encode($this);
    }
}
