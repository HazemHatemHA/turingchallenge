<?php

namespace TuringChallenge\Errors;

use Illuminate\Http\Request;

class NotFound extends Error
{

    public $message;

    function __construct($code,Request $request)
    {
        $str_arr = preg_split("/\//", $request->path());
        $this->message = $str_arr[0] . "(" . $str_arr[1] . ")" . " Don't exist ";
        $this->code = $code;
        unset($this->field);
        unset($this->status);
    }
}
