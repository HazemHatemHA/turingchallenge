<?php
namespace App\Http\Resources;
use App\Http\Resources\ProductResource;
use App\Http\Resources\AttributeValueResource;

use Illuminate\Http\Resources\Json\JsonResource;
class ProductAttributeResource extends JsonResource {
/**
 * Transform the resource into an array.
 *
 * @param  IlluminateHttpRequest  $request
 * @return array
 */
public function toArray($request) {
/* for one record 
 
   return new ProductAttributeResource(ProductAttribute::find($id)); 

 for multi records 

return ProductAttributeResource::collection(ProductAttribute::all());
 */ 
return [
'product_id' => $this->product_id,
'attribute_value_id' => $this->attribute_value_id,

'product'=> ProductResource::collection($this->whenLoaded('product')),
'attribute_value'=> AttributeValueResource::collection($this->whenLoaded('attribute_value')),
'created_at' => $this->created_at,
'updated_at' => $this->updated_at
];
}
}
