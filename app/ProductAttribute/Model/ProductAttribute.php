<?php

namespace TuringChallenge\ProductAttribute\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{

    protected $table = 'product_attribute';
    protected $fillable = ['product_id', 'attribute_value_id'];

    protected $appends = ['attr_value'];

    public function product()
    {
        return $this->belongsTo('TuringChallenge\Product\Model\Product');
    }

    public function getAttrValueAttribute() {
        return $this->attribute_value->attribute;
    }


    public function attribute_value()
    {
        return $this->belongsTo('TuringChallenge\AttributeValue\Model\AttributeValue','attribute_value_id');
    }
}
