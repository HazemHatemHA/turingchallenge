<?php

namespace TuringChallenge\Core\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TuringChallenge\Core\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAttributeRoutes();


        $this->mapAuditRoutes();

        $this->mapCategoryRoutes();

        $this->mapCustomerRoutes();

        $this->mapDepartmentRoutes();

        $this->mapOrderRoutes();

        $this->mapProductRoutes();

        $this->mapReviewRoutes();

        $this->mapShippingRoutes();

        $this->mapTaxRoutes();


        $this->mapPaymentsRoutes();

        $this->mapShoppingCartRoutes();


        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapAttributeRoutes()
    {
        Route::prefix('attributes')
        ->middleware('web')
        ->namespace('TuringChallenge\Attribute\Http\Controllers')
        ->group(base_path('app/Attribute/route.php'));

    }


    protected function mapAuditRoutes()
    {
        Route::prefix('audits')
        ->middleware('web')
        ->namespace('TuringChallenge\Audit\Http\Controllers')
        ->group(base_path('app/Audit/route.php'));

    }


    protected function mapCategoryRoutes()
    {
        Route::prefix('categories')
        ->middleware('web')
        ->namespace('TuringChallenge\Category\Http\Controllers')
        ->group(base_path('app/Category/route.php'));

    }

    protected function mapCustomerRoutes()
    {
        Route::middleware('web')
        ->namespace('TuringChallenge\Customer\Http\Controllers')
        ->group(base_path('app/Customer/route.php'));

    }

    protected function mapDepartmentRoutes()
    {
        Route::prefix('departments')
        ->middleware('web')
        ->namespace('TuringChallenge\Department\Http\Controllers')
        ->group(base_path('app/Department/route.php'));

    }

    protected function mapOrderRoutes()
    {
        Route::prefix('orders')
        ->middleware('web')
        ->namespace('TuringChallenge\Order\Http\Controllers')
        ->group(base_path('app/Order/route.php'));

    }
    protected function mapProductRoutes()
    {
        Route::prefix('products')
        ->middleware('web')
        ->namespace('TuringChallenge\Product\Http\Controllers')
        ->group(base_path('app/Product/route.php'));

    }

    protected function mapReviewRoutes()
    {
        Route::prefix('review')
        ->middleware('web')
        ->namespace('TuringChallenge\Review\Http\Controllers')
        ->group(base_path('app/Review/route.php'));

    }

    protected function mapShippingRoutes()
    {
        Route::prefix('shipping')
        ->middleware('web')
        ->namespace('TuringChallenge\ShippingRegion\Http\Controllers')
        ->group(base_path('app/ShippingRegion/route.php'));

    }

    protected function mapShoppingCartRoutes()
    {
        Route::prefix('shoppingcart')
        ->middleware('web')
        ->namespace('TuringChallenge\ShoppingCart\Http\Controllers')
        ->group(base_path('app/ShoppingCart/route.php'));

    }

    protected function mapTaxRoutes()
    {
        Route::prefix('tax')
        ->middleware('web')
        ->namespace('TuringChallenge\Tax\Http\Controllers')
        ->group(base_path('app/Tax/route.php'));

    }


    protected function mapPaymentsRoutes()
    {
        Route::middleware('web')
        ->namespace('TuringChallenge\Payments\Http\Controllers')
        ->group(base_path('app/Payments/route.php'));

    }



 //
}
