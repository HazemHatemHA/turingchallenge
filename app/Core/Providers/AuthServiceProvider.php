<?php

namespace TuringChallenge\Core\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'TuringChallenge\Model' => 'TuringChallenge\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Passport::tokensExpireIn(now()->addHour(24));
        Passport::refreshTokensExpireIn(now()->addDays(7));
    }
}
