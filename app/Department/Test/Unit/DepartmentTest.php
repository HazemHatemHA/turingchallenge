<?php

namespace TuringChallenge\Department\Test\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\Department\Http\Resources\DepartmentResource;
use TuringChallenge\Department\Model\Department;

use TuringChallenge\Core\traits\Singleton;

class DepartmentTest extends TestCase
{
    use Singleton;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    function setUp()
    {
        parent::setUp();
    }
    public function testAllDepartments()
    {
        $response = $this->get('departments');
        $data = DepartmentResource::collection(Department::all());
        $data = $data->toArray($data);
        $response
            ->assertStatus(200)
            ->assertExactJson($data);

    }

    public function testSpecificDepartment()
    {
        $item = Department::inRandomOrder()->first();
        $response = $this->get('departments/'.$item->department_id);

        $item = $item->toArray($item);

        $response
            ->assertStatus(200)
            ->assertExactJson($item);
    }
}
