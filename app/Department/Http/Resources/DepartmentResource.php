<?php
namespace TuringChallenge\Department\Http\Resources;
use TuringChallenge\Category\Http\Resources\CategoryResource;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  IlluminateHttpRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        /* for one record

   return new DepartmentResource(Department::find($id));

 for multi records

return DepartmentResource::collection(Department::all());
 */
        return [
            'department_id'=>$this->department_id,
            'name' => $this->name,
            'description' => $this->description,
           // 'category' => CategoryResource::collection($this->whenLoaded('category'))
        ];
    }
}
