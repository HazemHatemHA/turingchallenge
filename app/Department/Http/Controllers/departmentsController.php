<?php

namespace TuringChallenge\Department\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Department\Model\Department;

use TuringChallenge\Core\Http\Controllers\Controller;
use TuringChallenge\Errors\NotFound;
use TuringChallenge\Errors\Error;

use TuringChallenge\Department\Http\Resources\DepartmentResource;


class departmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allDepartments()
    {
        $data = DepartmentResource::collection(Department::all());
        $data = $data->toArray($data);
        return $data;
    }

    public function specificDepartment(Request $request, $department​_id)
    {

        $item = Department::find($department​_id);
        if ($item == null) {
            $error = new NotFound("DEP_01",$request);
            return $error->json();
        }
        return $item;
    }
}
