<?php

namespace TuringChallenge\Department\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';
    protected $primaryKey  = 'department_id';
    protected $fillable = ['name', 'description'];


    public function category()
    {
        return $this->hasMany('TuringChallenge\Category\Model\Category','category_id');
    }
}
