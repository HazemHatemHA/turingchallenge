<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'taxController@allTaxes');

Route::get('/{tax_id}', 'taxController@specificTax')
    ->where(['tax_id' => '[0-9]+']);
