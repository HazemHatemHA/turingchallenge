<?php

namespace TuringChallenge\Tax\Test\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\Tax\Model\Tax;

class TaxTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAllTaxes()
    {
        $response = $this->get('tax/');
        $result = \json_decode($response->getContent());
        if (sizeof($result) > 0) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure([['tax_id', 'tax_type','tax_percentage']]);
        }
        else $this->checkResultIsNull($response);
    }



    public function testTax()
    {
        $item = Tax::inRandomOrder()->first();
        if ($item) {
            $response = $this->get('tax/' . $item->tax_id);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['tax_id', 'tax_type', 'tax_percentage']
                );
        } else {
            $response = $this->get('tax/1');
            $this->checkResultIsNull($response);
        }
    }

    public function checkResultIsNull($response)
    {
        $result = \json_decode($response->getContent());
        if (sizeof($result) > 0) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure(['shipping_region_id', 'shipping_region']);
        }
        $response->assertOk();
    }
}
