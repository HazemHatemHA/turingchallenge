<?php

namespace TuringChallenge\Tax\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;
use  TuringChallenge\Tax\Model\Tax;
class taxController extends Controller
{
    public function allTaxes()
    {
      $data = Tax::all();
      return $data;
    }

    public function specificTax(Request $request, $tax_id)
    {
        $data = Tax::find($tax_id);
        return $data;
    }
}
