<?php

namespace TuringChallenge\Tax\Model;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'tax';
    protected $primaryKey  = 'tax_id';
    protected $fillable = ['tax_type', 'tax_percentage'];
}
