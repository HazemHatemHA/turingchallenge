<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/{category_id}', 'CategoryController@specificCategory')->where('category_id', '[0-9]+');


Route::get('/inProduct/{product_id}', 'CategoryController@categoriesProduct')->where(['product_id' => '[0-9]+']);


Route::get('/inDepartment/{department_id}', 'CategoryController@categoriesDepartment')->where(['department_id' => '[0-9]+']);

Route::get('/', 'CategoryController@allCategories');
