<?php

namespace TuringChallenge\Category\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey  = 'category_id';
    protected $fillable = ['department_id', 'name', 'description'];


    public function department()
    {
        return $this->belongsTo('TuringChallenge\Department\Model\Department', 'department_id');
    }
    public function product_category()
    {
        return $this->hasMany('TuringChallenge\ProductCategory\Model\ProductCategory', 'category_id');
    }
    public function products()
    {
        return $this->hasManyThrough(
            'TuringChallenge\Product\Model\Product',
            'TuringChallenge\ProductCategory\Model\ProductCategory',
            'category_id',
            'product_id',
            'category_id');
    }

    public function inProduct(){
        return $this->belongsToMany(
            '\TuringChallenge\Product\Model\Product',
            '\TuringChallenge\ProductCategory\Model\ProductCategory'
        );
    }
}
