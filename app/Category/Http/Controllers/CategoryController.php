<?php

namespace TuringChallenge\Category\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;


use TuringChallenge\Category\Model\Category;

use TuringChallenge\Errors\NotFound;
use TuringChallenge\Category\Http\Resources\CategoryResource;
use TuringChallenge\Errors\Unauthorized;
use TuringChallenge\Errors\Error;

use TuringChallenge\ProductCategory\Model\ProductCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function allCategories(Request $request)
    {
        $optional = new \stdClass();
        $optional->page = 1;
        $optional->limit = 20;
        $optional->order = null;
        $data = null;

        if (sizeof($request->query()) > 0) {

            if ($request->has('page')) {
                $optional->page = $request->input('page');
            }
            if ($request->has('limit')) {
                $optional->limit = $request->input('limit');
            }
            if ($request->has('order')) {
                $optional->order = $request->input('order');
            }

            $validator = \Validator::make((array)$optional, [
                'page' => 'integer',
                'limit' => 'integer',
            ], [
                'integer' => 'The field :attribute  is must be integer.'
            ]);

            if ($validator->fails()) {
                $field = $validator->errors()->keys()[0];
                $message = $validator->errors()->first();
                $error = new Error("CAT_02", $message, $field, 422);
                return $error->json();
            }

            if ($optional->order) {

                if ($optional->order == 'category_id' || $optional->order == 'name') {

                    $data = Category::Orderby($optional->order, 'asc')->paginate($optional->limit, ['*'], 'page',  $optional->page);

                } else {
                    $error = new Unauthorized("PAG_02", "the field " . $optional->order . " not allow sorting.", $optional->order);
                    return $error->json();
                }
            }
        }


        $count = Category::count();


        if ($data == null)
            $data = CategoryResource::collection(Category::paginate($optional->limit, ['*'], 'page', $optional->page));
        return response()->json(['count' => $count, 'rows' => $data->items()]);
    }

    public function specificCategory(Request $request, $category_id)
    {
        //
        $item = Category::find($category_id);
        if ($item == null) {
            $error = new NotFound("CAT_01",$request);
            return $error->json();
        }
        return $item;
    }


    public function categoriesProduct(Request $request, $product_id)
    {
        $records = ProductCategory::Where('product_id', $product_id)->get();
        if ($records == null) {
            $error = new NotFound("CAT_01",$request);
            return $error->json();
        }

        $categories = $records->map(function ($item, $key) {
            return $item->category;
        });


        $filteredNull = $categories->reject(function ($value, $key) {
            unset($value->description);
            return $value == null;
        });


        return $filteredNull->all();
    }


    public function categoriesDepartment(Request $request, $department_id)
    {
        //
        /*
       $items = Department::find($department_id);
         return $items->category;
*/

        $items = Category::Where('department_id', $department_id)->get();
        if ($items == null) {
            $error = new NotFound("DEP_02",$request);
            return $error->json();
        }
        return  $items;
    }
}
