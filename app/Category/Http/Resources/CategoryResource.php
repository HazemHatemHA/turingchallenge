<?php
namespace TuringChallenge\Category\Http\Resources;

use TuringChallenge\Department\Http\ResourcesDepartmentResource;
use TuringChallenge\Product\Http\Resources\ProductCategoryResource;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  IlluminateHttpRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        /* for one record

   return new CategoryResource(Category::find($id));

 for multi records

return CategoryResource::collection(Category::all());
 */
        return [
            'category_id'=>$this->category_id,
            'name' => $this->name,
            'description' => $this->description,
            'department_id' => $this->department_id,

            //   'department' => DepartmentResource::collection($this->whenLoaded('department')),
            //     'product_category' => ProductCategoryResource::collection($this->whenLoaded('product_category')),
        ];
    }
}
