<?php

namespace TuringChallenge\Category\Test\Unit;

use Tests\TestCase;
use TuringChallenge\Category\Model\Category;
use TuringChallenge\Department\Model\Department;
use TuringChallenge\ProductCategory\Model\ProductCategory;
use TuringChallenge\Core\traits\Singleton;

class CategoryTest extends TestCase
{

    use Singleton;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    function setUp()
    {
        parent::setUp();
    }
    public function testAllCategories()
    {
        $response = $this->get('categories');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['count', 'rows']);
    }

    public function testSpecificCategory()
    {
        $item = Category::inRandomOrder()->first();
        $response = $this->get('categories/' . $item->category_id);

        $response
            ->assertStatus(200)
            ->assertJsonStructure(['category_id', 'department_id', 'name', 'description']);
    }

    public function testCategoriesProduct()
    {
        $item = ProductCategory::first();
        $response = $this->get('categories/inProduct/' . $item->product_id);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([['category_id', 'department_id', 'name']]);
    }

    public function testCategoriesDepartment()
    {
        $item = Department::inRandomOrder()->first();
        $response = $this->get('categories/inDepartment/' . $item->department_id);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([['category_id', 'department_id', 'name', 'description']]);
    }
}
