<?php

namespace TuringChallenge\Order\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;
use TuringChallenge\Order\Model\Order;
use TuringChallenge\ShoppingCart\Http\Controllers\shoppingCartController;
use TuringChallenge\Errors\Error;
class ordersController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:api');
    }

    private function sendEmail($userData, $cart_id)
    {



            $to_name = $userData->name;
            // azez.2080.a1@gmail.com
            $to_email = "hazemght2013@gmail.com"; //$userData->email;
            $products = shoppingCartController::getInstance()->productsInCart($cart_id);
            $data = array('name' => $userData->name, "products" => $products);

            \Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('Turing Store confirm your order');
                $message->from("no-replay@store.com", 'Turing Store');
                $swiftMessage = $message->getSwiftMessage();

                $headers = $swiftMessage->getHeaders();
                $headers->addTextHeader('From', '<' . env('MAIL_USERNAME') . '>\n');
                $headers->addTextHeader('MIME-Version', ' 1.0\n');
                $headers->addTextHeader('Content-type', 'text/html');
                $headers->addTextHeader('charset=iso', '8859-1');
            });

    }

    public function createOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'cart_id' => 'required|string',
            'shipping_id' => 'required|integer',
            'tax_id' => 'required|integer',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_02", $message, $field, 500);
            return $error->json();
        }
        $cart_id = $request['cart_id'];
        unset($request['cart_id']);
        $request->request->add(['customer_id' => \Auth::guard('api')->user()->customer_id]);
        $order = Order::firstOrCreate($request->all());
        $orderID['order_id'] = $order->order_id;
        try {
        $this->sendEmail(\Auth::guard('api')->user(), $cart_id);
    } catch (\Exception $e) {
        $error = new Error("CONF_01",$e->getMessage(),"sendEmail",500);
        return $error->json();
    }
        \Log::info("crate order by " . \Auth::guard('api')->user()->email);

        return response()->json($orderID);
    }
    public function InfoAboutOrder($order_id)
    {
        $data = Order::find($order_id);
        $data = $data->order_detail;
        if ($data) {
            $data['subtotal'] = $data->quantity * $data->unit_cost;
            $data->makeHidden(['item_id']);
        }
        // subtotal
        return $data;
    }
    public function inCustomer()
    {
        $customer = \Auth::guard('api')->user();
        return $customer->order;
    }
    public function shortDetailOrder($order_id)
    { // order_detail
        $data = Order::find($order_id);
        if ($data) {
            $data->makeHidden(['comments', 'customer_id', 'auth_code', 'reference', 'shipping_id', 'tax_id', 'customer']);
            if ($data->customer) {
                $name = $data->customer->name;
                $data = $data->toArray();
                $data['name'] = $name;
            }
            return $data;
        }
        return $data;
    }
}
