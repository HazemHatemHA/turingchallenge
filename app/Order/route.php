<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', 'ordersController@createOrder');


Route::get('/{order_id}', 'ordersController@InfoAboutOrder')
    ->where(['order_id' => '[0-9]+']);

Route::get('/inCustomer', 'ordersController@inCustomer');

Route::get('/shortDetail/{order_id}', 'ordersController@shortDetailOrder')
    ->where(['order_id' => '[0-9]+']);
