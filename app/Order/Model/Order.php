<?php

namespace TuringChallenge\Order\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey  = 'order_id';
    protected $fillable = ['total_amount', 'created_on', 'shipped_on', 'status', 'comments', 'customer_id', 'auth_code', 'reference', 'shipping_id', 'tax_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    public function customer()
    {
        return $this->belongsTo('TuringChallenge\Customer\Model\Customer','customer_id');
    }
    public function shipping()
    {
        return $this->belongsTo('TuringChallenge\Shipping\Model\Shipping','order_id');
    }
    public function order_detail()
    {
        return $this->hasOne('TuringChallenge\OrderDetail\Model\OrderDetail','order_id');
    }
}
