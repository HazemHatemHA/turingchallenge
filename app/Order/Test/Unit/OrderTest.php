<?php

namespace TuringChallenge\Order\Test\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


use TuringChallenge\Order\Model\Order;
use TuringChallenge\Customer\Model\Customer;
use TuringChallenge\Shipping\Model\Shipping;
use TuringChallenge\ShoppingCart\Model\ShoppingCart;
use TuringChallenge\Tax\Model\Tax;


class OrderTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateOrder()
    {
        $customer = Customer::inRandomOrder()->first();
        $shipping = Shipping::inRandomOrder()->first();
        $shoppingCart = ShoppingCart::inRandomOrder()->first();
        $tax = Tax::inRandomOrder()->first();

        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('POST', 'orders', [
                'cart_id' => $shoppingCart->cart_id,
                'shipping_id' => $shipping->shipping_id,
                'tax_id' => $tax->tax_id,

            ]);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(['order_id']);
        } else {
            $response = $this->call('POST', 'orders');
            $this->assertEquals(401, $response->status());
        }
    }
    public function testInfoAboutOrder()
    {
        $customer = Customer::inRandomOrder()->first();
        $order = Order::inRandomOrder()->first();
        if ($order == null) {
            $this->assertTrue(true);
            return;
        }
        if ($customer) {
            $response = $this->actingAs($customer, 'api')
                ->json('GET', 'orders/' . $order->order_id);

            $obj = json_decode($response->getContent());
            if ($obj == null) {
                $response->assertOk();
                return;
            }
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['order_id', 'product_id', 'attributes', 'product_name', 'quantity', 'unit_cost', 'subtotal']]
                );
            return;
        } else {
            $response = $this->call('GET', 'orders/' . $order->order_id);
            $this->assertEquals(401, $response->status());
        }
    }
    public function testInCustomer()
    {
        $customer = Customer::inRandomOrder()->first();

        if ($customer) {
            $response = $this->actingAs($customer, 'api')
                ->json('GET', 'orders/inCustomer');

            $obj = json_decode($response->getContent());
            if (sizeof($obj) == 0) {
                $response->assertOk();
                return;
            }

            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['order_id', 'created_on', 'shipped_on', 'status', 'comments', 'auth_code', 'reference', 'shipping_id', 'tax_id', 'total_amount']]
                );
        } else {
            $response = $this->call('GET', 'orders/inCustomer');
            $this->assertEquals(401, $response->status());
        }

        return;
    }
    public function testShortDetailOrder()
    {
        $customer = Customer::inRandomOrder()->first();
        $order = Order::inRandomOrder()->first();
        if ($order == null) {
            $this->assertTrue(true);
            return;
        }
        if ($customer) {
            $response = $this->actingAs($customer, 'api')
                ->json('GET', 'orders/shortDetail/' . $order->order_id);
            $obi  = json_decode($response->getContent());


            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['order_id', 'total_amount', 'created_on', 'shipped_on', 'status', 'name']
                );
        } else {
            $response = $this->call('GET', 'orders/shortDetail/' . $order->order_id);
            $this->assertEquals(401, $response->status());
        }
    }
}
