<?php

namespace TuringChallenge\ProductCategory\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_category';
    protected $primaryKey  = 'id';
    protected $fillable = ['product_id', 'category_id'];


    public function product()
    {
        return $this->belongsTo('TuringChallenge\Product\Model\Product','category_id','product_id');
    }
    public function category()
    {
        return $this->belongsTo('TuringChallenge\Category\Model\Category','category_id');
    }
}
