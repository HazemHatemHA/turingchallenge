<?php

namespace TuringChallenge\AttributeValue\Model;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $table = 'attribute_value';
    protected $primaryKey  = 'attribute_value_id';
    protected $fillable = ['attribute_id','value'];


    public function product_attribute()
    {
        return $this->hasMany('TuringChallenge\ProductAttribute\Model\ProductAttribute');
    }
    public function attribute()
    {
        return $this->belongsTo('TuringChallenge\Attribute\Model\Attribute','attribute_id');
    }
}
