<?php
namespace TuringChallenge\AttributeValue\Http\Resources;

use TuringChallenge\ProductAttribute\Http\Resources\ProductAttributeResource;
use TuringChallenge\Attribute\Http\Resources\AttributeResource;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeValueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  IlluminateHttpRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        /* for one record

   return new AttributeValueResource(AttributeValue::find($id));

 for multi records

return AttributeValueResource::collection(AttributeValue::all());
 */
        return [
            'attribute_value_id'=>$this->attribute_value_id,
            'attribute_id' => $this->attribute_id,
            'value'=>$this->value,

            //  'product_attribute' => //ProductAttributeResource::collection($this->whenLoaded('product_attribute')),
            //  'attribute' => AttributeResource::collection($this->whenLoaded('attribute')),
            //  'created_at' => $this->created_at,
            //  'updated_at' => $this->updated_at
        ];
    }
}
