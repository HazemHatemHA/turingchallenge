<?php

namespace TuringChallenge\OrderDetail\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    protected $primaryKey  = 'item_id';
    protected $fillable = ['order_id', 'product_id', 'attributes', 'product_name', 'quantity', 'unit_cost'];


    public function order()
    {
        return $this->belongsTo('TuringChallenge\Order\Model\Order');
    }
    public function product()
    {
        return $this->belongsTo('TuringChallenge\Product\Model\Product');
    }
}
