<?php

namespace TuringChallenge\Search;

trait FullTextSearch
{
    /**
     * Replaces spaces with full text search wildcards
     *
     * @param string $term
     * @return string
     */
    protected function fullTextWildcards($term, $allWords = "on")
    {
        // removing symbols used by MySQL

        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $term = str_replace($reservedSymbols, '', $term);

        $words = explode(' ', $term);

        foreach ($words as $key => $word) {
            /*
             * applying + operator (required word) only big words
             * because smaller ones are not indexed by mysql
             */
            if (strlen($word) >= 3) {
                $words[$key] = '+' . $word . '*';
            }
        }
        $searchTerm = implode(' ', $words);
        return $searchTerm;
    }

    /**
     * Scope a query that matches a full text search of term.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $term
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $term, $allWords = "on")
    {
        $columns = implode(',', $this->searchable);

        //   dd($this->fullTextWildcards($term,$allWords));
        if ($allWords == "on") {

            $query->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($term, $allWords));
        } else {
            $query->whereRaw("MATCH ({$columns}) AGAINST (?)", $this->fullTextWildcards($term, $allWords));
        }

        return $query;
    }
}
