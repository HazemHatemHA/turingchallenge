<?php

namespace TuringChallenge\ShoppingCart\Model;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = 'shopping_cart';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey  = 'item_id';
    protected $fillable = ['cart_id', 'product_id', 'attributes', 'quantity', 'buy_now', 'added_on'];


    public function product()
    {
        return $this->belongsTo('TuringChallenge\Product\Model\Product','product_id');
    }
}
