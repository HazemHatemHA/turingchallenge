<?php

namespace TuringChallenge\ShoppingCart\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;

use TuringChallenge\ShoppingCart\Model\ShoppingCart;
use TuringChallenge\Errors\Error;
use TuringChallenge\Core\traits\Singleton;

class shoppingCartController extends Controller
{
    use Singleton;
    private function uniqidReal($length = 13)
    {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptoGraphically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }


    public function generateUniqueID()
    {
        $JsonObject['cart_id'] = $this->uniqidReal();

        return $JsonObject;
    }

    public function add(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'cart_id' => 'required|string|max:32',
            'attributes' => 'required|string|max:1000',
            'product_id' => 'required|integer',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_02", $message, $field, 500);
            return $error->json();
        }
        $isSave = true;

        $shoppingCart = ShoppingCart::Where([['cart_id', $request->cart_id], ['product_id', $request->product_id]])->first();
        if ($shoppingCart) {
            $shoppingCart->quantity += 1;
            $shoppingCart->added_on = \Carbon\Carbon::now();
            $isSave |= $shoppingCart->save();
        } else {
            $request->request->add(['added_on' => \Carbon\Carbon::now(), 'quantity' => 1]);
            $item = ShoppingCart::create($request->all());
            if ($item) {
                $isSave |= true;
            }
        }
        // dd($shoppingCart);
        if ($isSave)
            return $this->productsInCart($request->cart_id);
        else {
            $error = new Error("SCA_1", "can't add or update your card", "add", 500);
        }
    }
    public function productsInCart($cart_id)
    {
        $shoppingCart = ShoppingCart::Where('cart_id', $cart_id)->get()->makeHidden(['buy_now', 'added_on', 'product', 'cart_id']);

        $data = $shoppingCart->map(function ($item, $key) {
            if ($item->product) {
                $item['name'] = $item->product->name;
                $item['price'] = $item->product->price;
                $item['image'] = $item->product->image;
                $item['subtotal'] = $item->quantity * $item->product->price;
            }
            return $item;
        });

        return $data;
    }
    public function update(Request $request, $item_id)
    {

        $validator = \Validator::make($request->all(), [
            'quantity' => 'required|integer',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_02", $message, $field, 500);
            return $error->json();
        }
        $item = ShoppingCart::find($item_id);
        if ($item) {
            $item->quantity = $request->quantity;
            $item->save();
            return $this->productsInCart($item->cart_id);
        }
        $error = new Error("USR_2", "item does not exist ".$item_id, "ShoppingCart", 422);
        return $error->json();
    }
    public function empty(Request $request, $cart_id)
    {
        ShoppingCart::where('cart_id', $cart_id)->delete();
        return [];
    }
    public function moveToCart($item_id)
    {
        # code...
        $item = ShoppingCart::find($item_id);
        if ($item) {
            $item->added_on = \Carbon\Carbon::now();
            $item->buy_now = true;
            $item->save();
            return [];
        }
        $error = new Error("USR_2", "can't found item " . $item_id, "moveToCart", 500);
        return $error->json();
    }
    public function totalAmount($cart_id)
    {
        $products =  $this->productsInCart($cart_id);
        $total = 0;

        foreach ($products as $value) {

            $total += $value->subtotal;
        }
        $result['total_amount'] = $total;


        return  $result;
    }
    public function saveForLater($item_id)
    {
        $item = ShoppingCart::find($item_id);
        if ($item) {
            $item->quantity = 1;
            $item->buy_now = false;
            $item->save();
            return [];
        }
        $error = new Error("USR_2", "can't found item " . $item_id, "saveForLater", 500);
        return $error->json();
    }
    public function getSaved($cart_id)
    {
        $data = ShoppingCart::where([['cart_id', $cart_id], ['buy_now', false]])->with('product')->get();
        $data->makeHidden([
            'cart_id', 'product_id', 'quantity', 'buy_now', 'added_on',
            'product'
        ]);

        $data = $data->map(function ($item, $key) {
            $item['name'] = $item->product->name;
            $item['price'] = $item->product->price;
            return $item;
        });


        return $data;
    }
    public function removeProduct($item_id)
    {
        ShoppingCart::where('item_id', $item_id)->delete();
        return [];
    }
}
