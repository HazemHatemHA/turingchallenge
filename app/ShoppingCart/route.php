<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/generateUniqueId', 'shoppingCartController@generateUniqueID');


Route::post('/add', 'shoppingCartController@add');

Route::get('/{cart_id}', 'shoppingCartController@productsInCart');


Route::put('/update/{item_id}', 'shoppingCartController@update')
->where(['item_id' => '[0-9]+']);

Route::delete('/empty/{cart_id}', 'shoppingCartController@empty');
Route::get('/moveToCart/{item_id}', 'shoppingCartController@moveToCart')
->where(['item_id' => '[0-9]+']);


Route::get('/totalAmount/{cart_id}', 'shoppingCartController@totalAmount')
;
Route::get('/saveForLater/{item_id}', 'shoppingCartController@saveForLater')
->where(['item_id' => '[0-9]+']);


Route::get('/getSaved/{cart_id}', 'shoppingCartController@getSaved');

Route::delete('/removeProduct/{item_id}', 'shoppingCartController@removeProduct')
->where(['item_id' => '[0-9]+']);


