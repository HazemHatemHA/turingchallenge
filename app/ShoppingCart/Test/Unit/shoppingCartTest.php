<?php

namespace TuringChallenge\ShoppingCart\Test\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\ShoppingCart\Model\ShoppingCart;
use TuringChallenge\Product\Model\Product;
use function GuzzleHttp\json_decode;
use TuringChallenge\Core\traits\Singleton;

class shoppingCartTest extends TestCase
{
    use WithFaker,Singleton;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    function setUp()
    {
        parent::setUp();
    }
    public function testGenerateUniqueID()
    {
        $response = $this->get('shoppingcart/generateUniqueId');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['cart_id']
            );
    }

    public function testAdd()
    {
        $response = $this->get('shoppingcart/generateUniqueId');
        $result = json_decode($response->getContent());
        $continue = $this->checkResultIsNull($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['cart_id']
            );
        if (!$continue) {
            $product = Product::inRandomOrder()->first()->product_id;
            $response = $this->post('/shoppingcart/add', [
                'cart_id' => $result->cart_id,
                'product_id' => $product,
                'attributes' => 'Test from Unit Test',
            ]);
            $response->assertOk();
        }
    }

    public function testProductsInCart()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->get('shoppingcart/' . $record->cart_id);
        $continue = $this->checkResultIsNull($response);
        if (!$continue) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['item_id', 'name', 'image', 'product_id', 'attributes', 'price', 'quantity', 'subtotal']]
                );
        }
    }

    public function testUpdate()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->put('shoppingcart/update/' . $record->item_id, [
            'item_id' => $record->item_id,
            'quantity' => $this->faker->numberBetween($min = 1, $max = 5),
        ]);
        $continue = $this->checkResultIsNull($response);
        if (!$continue) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    [['item_id', 'name', 'image', 'product_id', 'attributes', 'price', 'quantity', 'subtotal']]
                );
        }
    }
    public function testEmpty()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->delete('shoppingcart/empty/' . $record->cart_id);
        $response->assertOk();
    }
    public function testMoveToCart()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->get('shoppingcart/moveToCart/' . $record->item_id);
        $response->assertOk();
    }
    public function testTotalAmount()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->get('shoppingcart/totalAmount/' . $record->cart_id);
        $continue = $this->checkResultIsNull($response);
        if (!$continue) {
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['total_amount']
                );
        }
    }

    public function testSaveForLater()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->get('shoppingcart/saveForLater/' . $record->item_id);
        $response->assertOk();
    }

    public function testGetSaved()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->get('shoppingcart/getSaved/' . $record->cart_id);
        $this->checkResultIsNull($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                [['item_id', 'name', 'attributes', 'price']]
            );
    }
    public function testRemoveProduct()
    {
        $record = ShoppingCart::inRandomOrder()->first();
        $response = $this->delete('shoppingcart/removeProduct/' . $record->product_id);
        $response->assertOk();
    }

    public function checkResultIsNull($response)
    {
        $result = \json_decode($response->getContent());
        if ($result == null) {
            $response->assertOk();
            return true;
        }
        return false;
    }
}
