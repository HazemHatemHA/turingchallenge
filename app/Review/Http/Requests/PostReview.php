<?php

namespace TuringChallenge\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostReview extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return   \Auth::guard('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer',
            'review' => 'required|string',
            'rating' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'product_id.required' => 'A product_id is required',
            'review.required'  => 'A review is required',
            'rating.required'  => 'A rating is required',
            'product_id.integer'  => 'A message is required',
            'rating.integer'  => 'A message is required',
            'review.string'  => 'A review must be a string',
        ];
    }
}
