<?php

namespace TuringChallenge\Review\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $primaryKey  = 'review_id';
    protected $fillable = ['customer_id', 'product_id', 'review', 'rating', 'created_on'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    public function customer()
    {
        return $this->belongsTo('TuringChallenge\Customer\Model\Customer', 'customer_id');
    }
    public function product()
    {
        return $this->belongsTo('TuringChallenge\Product\Model\Product');
    }
}
