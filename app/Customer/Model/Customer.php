<?php

namespace TuringChallenge\Customer\Model;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $table = 'customer';
    protected $primaryKey  = 'customer_id';
    protected $fillable = ['name', 'email', 'password', 'credit_card', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone','facebook_access_token'];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];


    public function review()
    {
        return $this->hasMany('TuringChallenge\Review\Model\Review','review_id','customer_id');
    }
    public function order()
    {
        return $this->hasMany('TuringChallenge\Order\Model\Order','customer_id');
    }
    public function shipping_region()
    {
        return $this->belongsTo('TuringChallenge\ShippingRegion\Model\ShippingRegion','shipping_region_id','customer_id');
    }
}
