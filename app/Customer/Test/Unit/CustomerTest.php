<?php

namespace TuringChallenge\Customer\Test\Unit;;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use TuringChallenge\Customer\Model\Customer;
use TuringChallenge\Errors\Error;
use TuringChallenge\Core\traits\Singleton;


class CustomerTest extends TestCase
{
    use WithFaker,Singleton;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    function setUp()
    {
        parent::setUp();
    }
    public function testUpdateCustomer()
    {
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('PUT', 'customer', [
                'name' => $this->faker->name,
                'email' => $this->faker->email,
            ]);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card']
                );
        } else {
            $response = $this->call('PUT', 'customer');
            $this->assertEquals(401, $response->status());
        }
    }
    public function testInfoCustomer()
    {
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('GET', 'customer');
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card']
                );
        } else {
            $response = $this->call('GET', 'customer');
            $this->assertEquals(401, $response->status());
        }
    }
    public function testRegisterCustomer()
    {

        $response = $this->json('POST', 'customers', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => '123456'
        ]);
        $obj = json_decode($response->getContent());
        if ($obj instanceof \Error) {
            dd("AA");
        }
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['customer' => ['schema' => ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card'], 'accessToken', 'expires_in']]
            );
    }

    public function testLoginCustomer()
    {
        $response = $this->json('POST', 'customers/login', [
            'email' => "haz2@haz.com",
            'password' => "123456"
        ]);
        $obj = json_decode($response->getContent());
        if ($obj->code) {
            $response->assertStatus(200);
            return;
        }
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['customer' => ['schema' => ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card'], 'accessToken', 'expires_in']]
            );
    }

    public function testFacebookCustomer()
    {
        $response = $this->post('customers/facebook', ['access_token' => 'aa']);
        $obj = json_decode($response->getContent());
        if ($obj->code) {
            $response->assertStatus(200);
            return;
        }
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                ['customer' => ['schema' => ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card'], 'accessToken', 'expires_in']]
            );
    }
    public function testUpdateAddressCustomer()
    {
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('PUT', 'customer/address', [
                'address_1' => $this->faker->address,
                'city' => $this->faker->email,
                'region' => "Liberecký kraj",
                'postal_code' => $this->faker->postcode,
                'country' => $this->faker->country,
                'shipping_region_id' => 1,
            ]);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['customer_id', 'name', 'email', 'credit_card', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card']
                );
        } else {
            $response = $this->call('PUT', 'customer/address');
            $this->assertEquals(401, $response->status());
        }
    }
    public function testUpdateCreditCartCustomer()
    {
        $customer = Customer::inRandomOrder()->first();
        if ($customer) {
            $response = $this->actingAs($customer, 'api')->json('PUT', 'customer/creditCard', [
                'credit_card' => $this->faker->creditCardNumber,

            ]);
            $response
                ->assertStatus(200)
                ->assertJsonStructure(
                    ['customer_id', 'name', 'email', 'address_1', 'address_2', 'city', 'region', 'postal_code', 'country', 'shipping_region_id', 'day_phone', 'eve_phone', 'mob_phone', 'credit_card']
                );
        } else {
            $response = $this->call('PUT', 'customer/creditCard');
            $this->assertEquals(401, $response->status());
        }
    }
}
