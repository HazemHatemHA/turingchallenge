<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::put('customer','customersController@updateCustomer');

Route::get('customer','customersController@infoCustomer');

Route::post('customers','customersController@registerCustomer');

Route::post('customers/login','customersController@loginCustomer');

Route::post('customers/facebook','customersController@facebookCustomer');

Route::put('customer/address','customersController@updateAddressCustomer');

Route::put('customer/creditCard','customersController@updateCreditCartCustomer');



Route::get('auth/facebook', 'customersController@redirectToFacebook');
Route::get('auth/facebook/callback', 'customersController@handleFacebookCallback');

Route::get('login',function(){
    return 'you are in our system :D';
})->name('login');

Route::get('register',function(){
    return 'you are in our system :D';
})->name('register');


