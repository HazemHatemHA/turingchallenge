<?php

namespace TuringChallenge\Customer\Http\Controllers;

use Illuminate\Http\Request;
use TuringChallenge\Core\Http\Controllers\Controller;
use TuringChallenge\Errors\Error;
use TuringChallenge\Errors\Unauthorized;
use TuringChallenge\Customer\Model\Customer;
use LVR\CreditCard\CardNumber;

use Socialite;

class customersController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            "auth:api",
            ['except' =>
            ['registerCustomer', 'loginCustomer', 'facebookCustomer', 'redirectToFacebook', 'handleFacebookCallback']]
        );
    }
    private function ValidateMessage($validator)
    {
        $field = $validator->errors()->keys()[0];
        $message = $validator->errors()->first();
        $error = new Error("USR_00", $message, $field, 422);
        if (strpos($message, 'too long') !== true) {
            $error->code = "USR_07";
        }
        if (strpos($message, 'invalid phone number') !== true) {
            $error->code = "USR_06";
        }
        if (strpos($message, 'not number') !== true) {
            $error->code = "USR_09";
        }
        if (strpos($message, 'card_invalid') !== true) {
            $error->code = "USR_08";
        }
        return $error->json();
    }
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['password'] = $user->getId();
            $create['facebook_access_token'] = $user->token;

            $validator = \Validator::make($create, [
                'email' => 'unique:customer',
            ]);

            if ($validator->fails()) {
                $credentials['email'] = $create['email'];
                $credentials['password'] = $create['password'];
                if (\Auth::attempt($credentials)) {
                    return $this->authResponse(\Auth::user());
                }
            }
            $create['password'] = bcrypt($user->getId());
            $customer = Customer::firstOrCreate($create);
            $customer->save();
            return redirect()->route('login');
        } catch (Exception $e) {
            //    return redirect('auth/facebook');
        }
    }

    private function updateResponse($request, $code, $message, $field)
    {
        $customer = \Auth::guard('api')->user();
        $result = $customer->update($request->all());

        if ($result) {
            return $customer;
        }
        $error = new Error($code, $message, $field, 500);
        return $error->json();
    }

    public function updateCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|string|max:100',
            'password' => 'string|nullable',
            'day_phone' => 'string|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'eve_phone' => 'string|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'mob_phone' => 'string|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        ], [
            'required' => 'The field :attribute  is empty.',
            'max' => ' this is too long :attribute .',
            'regex' => ' :attribute invalid phone number..',

        ]);
        if ($validator->fails()) {
            return $this->ValidateMessage($validator);
        }
        if (\Auth::guard('api')->user()->email != $request->email) {
            $validator = \Validator::make($request->all(), [
                'email' => 'unique:customer',
            ], [
                'unique' => $request->email . ' has already been taken.'
            ]);

            if ($validator->fails()) {
                return $this->ValidateMessage($validator);
            }
        }

        return $this->updateResponse($request, "CUS_U1", "Can\'t Update name or email", "updateCustomer");
    }

    public function infoCustomer()
    {
        $customer = \Auth::guard('api')->user();

        return $customer;
    }

    public function registerCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|unique:customer|max:100',
            'password' => 'required|string',
        ], [
            'required' => 'The field :attribute  is empty.',
            'max' => 'this is too long :attribute'
        ]);

        if ($validator->fails()) {
            return $this->ValidateMessage($validator);
        }


        $input = $request->all();
        $input['password'] = bcrypt($input['password']);;
        $customer = Customer::create($input);
        $customer = Customer::find($customer->customer_id);



        return $this->authResponse($customer);
    }

    private function authResponse($model)
    {
        if ($model != null) {
            $success['schema'] = $model;
            $Token = $model->createToken('TuringChallenge');
            $success['accessToken'] =  "Bearer " . $Token->accessToken;

            $success['expires_in'] =
                gmdate(
                    "H",
                    $Token->token->expires_at->diffInSeconds(\Carbon\Carbon::now())
                ) . "h";

            return response()->json(['customer' => $success], 200);
        }


        $error = new Error("AUT_02", "accessToken does not exist in DB", "accessToken", 422);
        return $error->json();
    }

    public function loginCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|max:100',
            'password' => 'required|string',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_02", $message, $field, 500);
            return $error->json();
        }

        $credentials = $request->only('email', 'password');

        if (\Auth::attempt($credentials)) {
            \Log::info("Login to system" . \Auth::user()->email);
            return $this->authResponse( \Auth::user());
        }
        \Log::Info("Try to login system with a incorrect credentials with email " . $credentials['email'] . ".from " . $request->ip());
        $error = new Error("USR_01", "Email or Password is invalid", "'email', 'password'", 422);

        return $error->json();
    }

    public function facebookCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'accessToken' => 'required|string',
        ], [
            'required' => 'The field :attribute  is empty.'
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_02", $message, $field, 422);
            return $error->json();
        }

        $customer = Customer::Where('facebook_access_token', $request->accessToken)->first();
        return $this->authResponse($customer);




        return $error->json();
    }

    public function updateAddressCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'address_1' => 'required|string|max:100',
            'city' => 'required|string|max:100',
            'region' => 'required|string|max:100',
            'postal_code' => 'required|string|max:100',
            'country' => 'required|string|max:100',
            'shipping_region_id' => 'required|integer|max:100',
        ], [
            'required' => 'The field :attribute  is require.',
            'max' => 'this is too long :attribute ',
            'integer' => 'The :attribute is not number '
        ]);

        if ($validator->fails()) {
           return $this->ValidateMessage($validator);
        }
        \Log::info("update Address for customer" . \Auth::guard('api')->user()->email);
        return $this->updateResponse($request, "USR_02", "Can\'t Update address", "updateAddressCustomer");
    }

    public function updateCreditCartCustomer(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'credit_card' => ['required', new CardNumber],
        ], [
            'required' => 'The field :attribute  is required',
        ]);

        if ($validator->fails()) {
            $field = $validator->errors()->keys()[0];
            $message = $validator->errors()->first();
            $error = new Error("USR_00", $message, $field, 422);

            $error->code = "USR_02";
            return $error->json();
        }
        \Log::info("update Address for customer" . \Auth::guard('api')->user()->email);
        return $this->updateResponse($request, "USR_02", "Can\'t Update creditCard ", "updateCreditCartCustomer");
    }
}
