<?php

namespace Tests\Feature;

use Tests\TestCase;


use TuringChallenge\Department\Test\Unit\DepartmentTest;
use TuringChallenge\Category\Test\Unit\CategoryTest;
use TuringChallenge\shoppingCart\Test\Unit\shoppingCartTest;
use TuringChallenge\Customer\Test\Unit\CustomerTest;
use TuringChallenge\Product\Test\Unit\ProductTest;
use TuringChallenge\Payments\Test\Unit\StripeTest;
use TuringChallenge\Core\traits\Singleton;


class featureRequirementsTest extends TestCase
{
    use Singleton;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    function setUp()
    {
        parent::setUp();
    }

    public function testViewAllItems()
    {
        ProductTest::getInstance()->testAllProducts();
    }

    public function testItemsBasedCategory()
    {
        ProductTest::getInstance()->testProductsInCategory();
        CategoryTest::getInstance()->testAllCategories();
    }
    public function testItemsBasedDepartment()
    {
        ProductTest::getInstance()->testProductsInDepartment();
        DepartmentTest::getInstance()->testAllDepartments();
    }


    public function testSpecificItem()
    {
        ProductTest::getInstance()->testSpecificProduct();
        DepartmentTest::getInstance()->testSpecificDepartment();
        CategoryTest::getInstance()->testSpecificCategory();
    }

    public function testAddItemsShoppingCart()
    {
        shoppingCartTest::getInstance()->testAdd();
    } //

    public function testUpdateAndUpdateAddress()
    {

        CustomerTest::getInstance()->testUpdateAddressCustomer();
    }
    public function testCheckoutStripe()
    {
        StripeTest::getInstance()->testCheckOut();
    }
    public function testLogin()
    {
        // confirmations over emails
        CustomerTest::getInstance()->testLoginCustomer();
    }
    public function testConfirmEmail()
    {
        $this->assertTrue(true);
    }
    public function testClearShoppingCart()
    {
        shoppingCartTest::getInstance()->testEmpty();
    }

}
