<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\featureRequirementsTest;

class RequestTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test100()
    { /*
        for ($i=0; $i < 100; $i++) {
            # code...
            featureRequirementsTest::getInstance()->testViewAllItems();
            featureRequirementsTest::getInstance()->testItemsBasedCategory();
            featureRequirementsTest::getInstance()->testSearchBox();
            featureRequirementsTest::getInstance()->testPaging();
            featureRequirementsTest::getInstance()->testSpecificItem();
            featureRequirementsTest::getInstance()->testSpecificItem();
        }
        */
        $this->assertTrue(True);
    }

    public function test1000()
    {
        /*
        for ($i=0; $i < 1000; $i++) {
            featureRequirementsTest::getInstance()->testViewAllItems();
            featureRequirementsTest::getInstance()->testItemsBasedCategory();
            featureRequirementsTest::getInstance()->testSearchBox();
            featureRequirementsTest::getInstance()->testPaging();
            featureRequirementsTest::getInstance()->testSpecificItem();
            featureRequirementsTest::getInstance()->testSpecificItem();
        }
*/
        $this->assertTrue(True);
    }
    public function test10000()
    {

        for ($i = 0; $i < 10000; $i++) {
            featureRequirementsTest::getInstance()->testViewAllItems();
            featureRequirementsTest::getInstance()->testItemsBasedCategory();
            featureRequirementsTest::getInstance()->testSearchBox();
            featureRequirementsTest::getInstance()->testPaging();
            featureRequirementsTest::getInstance()->testSpecificItem();
            featureRequirementsTest::getInstance()->testSpecificItem();
        }

        $this->assertTrue(True);
    }
}
