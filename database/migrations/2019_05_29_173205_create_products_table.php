<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('product_id');
            $table->string('name', 100);
            $table->string('description', 1000);
            $table->string('image', 150)->nullable();
            $table->string('image_2', 150)->nullable();
            $table->string('thumbnail', 150)->nullable();
            $table->double('price', 10, 2);
            $table->double('discounted_price', 10, 2)->default(0.00);

            $table->smallInteger('display')->nullable()->default(0);


        });
        \DB::statement('ALTER TABLE product ADD FULLTEXT fulltext_index (name, description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
