<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('review_id');
            $table->integer('customer_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->text('review');
            $table->smallInteger('rating');
            $table->timestamp('created_on')->default(\DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('customer_id')->references('customer_id')->on('customer');

            $table->foreign('product_id')->references('product_id')->on('product');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review');
    }
}
