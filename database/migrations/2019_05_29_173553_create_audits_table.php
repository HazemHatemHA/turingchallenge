<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('audit_id');
            $table->integer('order_id')->unsigned();
            $table->timestamp('created_on')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->text('message');
            $table->integer('code');

            $table->foreign('order_id')->references('order_id')->on('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit');
    }
}
