<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('customer_id');
            $table->string('name', 50);
            $table->string('email', 100);
            $table->string('password');
            $table->text('credit_card')->nullable();
            $table->string('address_1', 100)->nullable();
            $table->string('address_2', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('region', 100)->nullable();
            $table->string('postal_code', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->integer('shipping_region_id')->default(1);
            $table->string('day_phone', 100)->nullable();
            $table->string('eve_phone', 100)->nullable(); //access_token
            $table->string('mob_phone', 100)->nullable();

            $table->text('facebook_access_token')->nullable();
            $table->text('google_access_token')->nullable();
            $table->text('twitter_access_token')->nullable();
            $table->text('github_access_token')->nullable();

            $table->unique('email');
            $table->foreign('shipping_region_id')->references('shipping_region_id')->on('shipping_region');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
