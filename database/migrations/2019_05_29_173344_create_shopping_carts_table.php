<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('item_id');
            $table->char('cart_id', 32);
            $table->integer('product_id')->unsigned();
            $table->string('attributes', 1000);
            $table->integer('quantity');
            $table->boolean('buy_now')->default(true);
            $table->dateTime('added_on');

            $table->foreign('product_id')->references('product_id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart');
    }
}
