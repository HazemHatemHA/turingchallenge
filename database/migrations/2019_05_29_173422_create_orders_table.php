<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('order_id');
            $table->double('total_amount', 10, 2)->default(0.00);

            $table->timestamp('created_on')->default(\DB::raw('CURRENT_TIMESTAMP'));

            $table->dateTime('shipped_on')->nullable();
            $table->integer('status')->default(0);

            $table->string('comments', 255)->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('auth_code', 50)->nullable();
            $table->string('reference', 50)->nullable();
            $table->integer('shipping_id')->unsigned()->nullable();
            $table->integer('tax_id')->unsigned()->nullable();

           $table->foreign('customer_id')->references('customer_id')->on('customer');

           $table->foreign('shipping_id')->references('shipping_id')->on('shipping');

           $table->foreign('tax_id')->references('tax_id')->on('tax');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
