<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('shipping_id');
            $table->string('shipping_type', 100);
            $table->double('shipping_cost', 10, 2);
            $table->integer('shipping_region_id')->unsigned();

            $table->foreign('shipping_region_id')->references('shipping_region_id')->on('shipping_region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
