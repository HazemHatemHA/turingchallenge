<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:"GET, POST, PUT, DELETE, OPTIONS"');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, X-Token-Auth, Authorization');

*/



Route::get('/', function () {
    return view('welcome');
});
 // Clear cache
Route::get('/db/seed', function () {
    echo "<p>Database seeding started...</p>";
    $exitCode = Artisan::call('db:seed',['--force' => true]);
    echo "<p>Database seeding completed.</p>";
});

Route::get('/cache/clear', function () {
    //

    echo "<p>key:generate  started...</p>";
    $exitCode = Artisan::call('key:generate',['--force' => true]);
    echo "<p>key:generate  completed.</p>";

    echo "<p>Clear cache  started...</p>";
    $exitCode = Artisan::call('cache:clear',['--force' => true]);
    echo "<p>Clear cache  completed.</p>";


    echo "<p>route:cache  started...</p>";
    $exitCode = Artisan::call('route:cache',['--force' => true]);
    echo "<p>route:cache  completed.</p>";

    echo "<p>config:cache  started...</p>";
    $exitCode = Artisan::call('config:cache',['--force' => true]);
    echo "<p>config:cache  completed.</p>";

    echo "<p>optimize  started...</p>";
    $exitCode = Artisan::call('optimize',['--force' => true]);
    echo "<p>Coptimize  completed.</p>";


});
